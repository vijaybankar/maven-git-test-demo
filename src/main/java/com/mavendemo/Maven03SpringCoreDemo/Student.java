/**
 * 
 */
package com.mavendemo.Maven03SpringCoreDemo;

/**
 * @author vijaybankar
 *
 */
public class Student {
	
	private String sName;
	private String sAddress;
	private String sEmail;
	private String sClass;
	
	public String getsName() {
		return sName;
	}
	public void setsName(String sName) {
		this.sName = sName;
	}
	public String getsAddress() {
		return sAddress;
	}
	public void setsAddress(String sAddress) {
		this.sAddress = sAddress;
	}
	public String getsEmail() {
		return sEmail;
	}
	public void setsEmail(String sEmail) {
		this.sEmail = sEmail;
	}
	
	public String getsClass() {
		return sClass;
	}
	public void setsClass(String sClass) {
		this.sClass = sClass;
	}
	@Override
	public String toString() {
		return "Student [sName=" + sName + ", sAddress=" + sAddress + ", sEmail=" + sEmail + ", sClass=" + sClass + "]";
	}
	
}
