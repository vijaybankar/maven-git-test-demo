package com.mavendemo.Maven03SpringCoreDemo;

public class HelloSpring {

	private String messageCode;
	private String message;
	
	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void show() {
		System.out.println("this is show method");
	}

	@Override
	public String toString() {
		return "HelloSpring [messageCode=" + messageCode + ", message=" + message + "]";
	}
	
	

}
