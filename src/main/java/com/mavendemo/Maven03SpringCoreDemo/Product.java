/**
 * 
 */
package com.mavendemo.Maven03SpringCoreDemo;

/**
 * @author vijaybankar
 *
 */
public class Product {
	
	private String prodName;
	private String prodType;
	private String prodNtpa;

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getProdNtpa() {
		return prodNtpa;
	}

	public void setProdNtpa(String prodNtpa) {
		this.prodNtpa = prodNtpa;
	}

	@Override
	public String toString() {
		return "Product [prodName=" + prodName + ", prodType=" + prodType + ", prodNtpa=" + prodNtpa + "]";
	}

	
}
