package com.mavendemo.Maven03SpringCoreDemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainCall {
	
	public static void main(String[] arg) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("HelloSpringMaven-Bean.xml");
		
		HelloSpring hello = (HelloSpring) context.getBean("springMavenMsg");
		hello.show();
		System.out.println(hello.toString());
		
		Student stud = (Student) context.getBean("student");
		System.out.println(stud.toString());
		
		User user = (User) context.getBean("user");
		System.out.println(user.toString());
		
		Item item = (Item) context.getBean("item");
		System.out.println(item.toString());
		
		Product product = (Product) context.getBean("product");
		System.out.println(product.toString());

		Voting voting = (Voting) context.getBean("voting");
		System.out.println(voting.toString());
		
	}

}
