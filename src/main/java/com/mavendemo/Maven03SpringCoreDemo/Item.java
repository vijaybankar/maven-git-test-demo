/**
 * 
 */
package com.mavendemo.Maven03SpringCoreDemo;

/**
 * @author vijaybankar
 *
 */
public class Item {
	
	private String itemId;
	private String itemName;
	private String itemType;
	private double itemPrice;

	public String getItemId() {
		return itemId;
	}

	public String getItemName() {
		return itemName;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		return "Item [itemId=" + itemId + ", itemName=" + itemName + ", itemType=" + itemType + ", itemPrice="
				+ itemPrice + "]";
	}
	
	
	
}
