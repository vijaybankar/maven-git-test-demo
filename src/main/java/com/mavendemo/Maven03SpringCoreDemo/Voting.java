/**
 * 
 */
package com.mavendemo.Maven03SpringCoreDemo;

/**
 * @author vijaybankar
 *
 */
public class Voting {
	
	private String accountId;
	private String entryType;
	
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getEntryType() {
		return entryType;
	}
	public void setEntryType(String entryType) {
		this.entryType = entryType;
	}
	@Override
	public String toString() {
		return "Voting [accountId=" + accountId + ", entryType=" + entryType + "]";
	}
	
	

}
